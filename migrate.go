package main

import (
	"trade-bot/src/db"
	"github.com/joho/godotenv"
	"log"
)

func main() {
	envErr := godotenv.Load()
	if envErr != nil {
		log.Fatal("Error loading .env file")
	}
	db.Migrate()
}
