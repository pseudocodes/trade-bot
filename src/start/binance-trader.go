package start

import (
	"github.com/pdepip/go-binance/binance"
	"trade-bot/src/db"
	"trade-bot/src/util"
)

var binanceTakerFee = 0.01

func OpenMarketPosition(trade *db.Trade, user db.User) (binance.PlacedOrder, error) {
	order := binance.MarketOrder{
		Symbol:   trade.Symbol,
		Side:     trade.Side,
		Type:     trade.Type,
		Quantity: trade.Quantity,
	}

	client := binance.New(
		user.Key,
		user.Secret,
	)

	res2, err := client.PlaceMarketOrder(order)
	if err != nil {
		util.LogError(err, "openMarket place order #456")
		return binance.PlacedOrder{}, err
	}

	updateTradeDetail(client, trade, res2, "BUY")

	return res2, err
}

func CloseMarketPosition(trade *db.Trade, user db.User) (binance.PlacedOrder, error) {
	var tradeDetail db.TradeDetail

	pgClient := util.GetPgClient()
	client := binance.New(
		user.Key,
		user.Secret,
	)

	if err := pgClient.First(&tradeDetail, trade.TradeDetailID).Error; err != nil {
		util.LogError(err, "closeMarket db search tradeDetail by `trade.TradeDetailID` #942 ")
		return binance.PlacedOrder{}, err
	}

	order := binance.MarketOrder{
		Symbol:   trade.Symbol,
		Side:     trade.Side,
		Type:     trade.Type,
		Quantity: tradeDetail.BuyExecutedQty - (tradeDetail.BuyExecutedQty * binanceTakerFee),
	}

	res2, err := client.PlaceMarketOrder(order)
	if err != nil {
		util.LogError(err, "closeMarket place order #253")
		return binance.PlacedOrder{}, err
	}

	updateTradeDetail(client, trade, res2, "SELL")

	return res2, err
}

// side - what do you want to do? BUY or SELL
func updateTradeDetail(client *binance.Binance, trade *db.Trade, placedOrder binance.PlacedOrder, side string) (db.TradeDetail, error) {
	pgClient := util.GetPgClient()
	var tradeDetail db.TradeDetail
	var orderCheck binance.OrderQuery
	switch side {
	case "BUY":
		orderCheck = binance.OrderQuery{Symbol: placedOrder.Symbol, OrderId: placedOrder.OrderId}
	case "SELL":
		orderCheck = binance.OrderQuery{Symbol: placedOrder.Symbol, OrderId: placedOrder.OrderId}
	}

	orderStatus, err := client.CheckOrder(orderCheck)
	if err != nil {
		util.LogError(err, "updateTradeDetail checkOrder #917")
		return db.TradeDetail{}, err
	}

	dbPlacedOrder := db.PlacedOrder{
		Symbol:        placedOrder.Symbol,
		OrderId:       placedOrder.OrderId,
		ClientOrderId: placedOrder.ClientOrderId,
		TransactTime:  placedOrder.TransactTime,
		Side:          side,
	}

	if err := pgClient.Create(&dbPlacedOrder).Error; err != nil {
		util.LogError(err, "CloseMarketPosition create dbPlacedOrder #203")
		return db.TradeDetail{}, err
	}

	switch side {
	case "BUY":
		tradeDetail = db.TradeDetail{
			TradeId:        trade.ID,
			BuyPrice:       orderStatus.Price,
			BuyExecutedQty: orderStatus.ExecutedQty,
		}
		if err := pgClient.Create(&tradeDetail).Error; err != nil {
			util.LogError(err, "updateTradeDetail switch BUY #806")
			return db.TradeDetail{}, err
		}
		trade.TradeDetailID = tradeDetail.ID

	case "SELL":
		if err := pgClient.First(&tradeDetail, trade.TradeDetailID).Error; err != nil {
			util.LogError(err, "updateTradeDetail switch SELL  search  tradeDetail by `trade.TradeDetailID` #476")
			return db.TradeDetail{}, err
		}

		if err := pgClient.Model(&tradeDetail).Updates(db.TradeDetail{
			SellPrice: orderStatus.Price, SellExecutedQty: orderStatus.ExecutedQty,
		}).Error; err != nil {
			util.LogError(err, "updateTradeDetail switch SELL update tradeDetail #846")
			return db.TradeDetail{}, err
		}
		if err := pgClient.Model(&trade).Updates(&db.Trade{
			IsDone:      true,
			CloseByStop: trade.CloseByStop,
		}).Error; err != nil {
			util.LogError(err, "updateTradeDetail switch SELL  search  tradeDetail #876")
			return db.TradeDetail{}, err
		}
	}
	return tradeDetail, err
}
