package start

import (
	"fmt"
	"github.com/gin-gonic/gin"
	"gopkg.in/validator.v2"
	"log"
	"net/http"
	"os"
	"trade-bot/src/db"
	"trade-bot/src/store"
	"trade-bot/src/util"
)

var botAccessHeader string

func Ping(c *gin.Context) {
	c.JSON(http.StatusOK, gin.H{"ping": "pong"})
}

func AddUser(c *gin.Context) {
	var user db.User
	c.BindJSON(&user)
	if user.Key != "" && user.Secret != "" && user.UserId != 0 {
		pgClient := util.GetPgClient()
		if pgClient.NewRecord(user) {
			if res := pgClient.Create(&user); res.Error != nil {
				c.JSON(http.StatusBadRequest, gin.H{"detail": "this user exists"})
			} else {
				c.JSON(http.StatusOK, user)
			}
		} else {
			c.JSON(http.StatusOK, user)
		}
	} else {
		c.JSON(http.StatusBadRequest, gin.H{"detail": "missed required fields\n"})
	}
}

func UpdateUser(c *gin.Context) {
	var data db.User
	var user db.User

	pgClient := util.GetPgClient()
	c.BindJSON(&data)

	if err := pgClient.Where("user_id = ?", c.Param("user_id")).First(&user).Error; err != nil {
		c.String(http.StatusBadRequest, "User not found or missed user_id")
	} else {
		if data.Key != "" {
			user.Key = data.Key
		}
		if data.Secret != "" {
			user.Secret = data.Secret
		}
		if err := pgClient.Save(&user).Error; err != nil {
			c.String(http.StatusBadRequest, "Can't update user")
		} else {
			c.JSON(http.StatusOK, &user)
		}
	}
}

func GetUser(c *gin.Context) {
	var user db.User
	pgClient := util.GetPgClient()

	if err := pgClient.Where("user_id = ?", c.Param("user_id")).First(&user).Error; err != nil {
		c.String(http.StatusNotFound, "user doesn't exist")
	} else {
		c.JSON(http.StatusOK, user)
	}
}

func ClearUser(c *gin.Context) {
	var user db.User
	pgClient := util.GetPgClient()

	if err := pgClient.Model(&user).Where("user_id = ?", c.Param("user_id")).Updates(db.User{
		Key: " ", Secret: " ",
	}).Error; err != nil {
		msg := fmt.Sprintf("can't reset key or secret on user, detail: %+v, #914", err.Error())
		util.LogError(err, msg)
		c.String(http.StatusBadRequest, msg)
		return
	}

	c.JSON(http.StatusOK, user)
}

func AddTrade(c *gin.Context) {
	var trade db.Trade
	var user db.User
	pgClient := util.GetPgClient()
	c.BindJSON(&trade)
	if errs := validator.Validate(trade); errs != nil {
		c.JSON(http.StatusBadRequest, gin.H{"detail": errs.Error()})
		return
	}

	if err := pgClient.Where("user_id = ?", trade.UserID).First(&user).Error; err != nil {
		c.JSON(http.StatusNotFound, gin.H{"detail": "user not found"})
		return
	}
	if res := pgClient.Create(&trade); res.Error != nil {
		c.JSON(http.StatusBadRequest, gin.H{"detail": "something went wrong"})
		return
	} else {
		store.AddTrade(store.GetWaitingTradesStore(), trade)
	}
	c.JSON(http.StatusOK, trade)
}

func CancelTrade(c *gin.Context) {
	var trade db.Trade
	var tradeDetail db.TradeDetail
	pgClient := util.GetPgClient()
	if err := pgClient.Where("user_id = ? AND id = ?", c.Param("user_id"), c.Param("trade_id")).First(&trade).Error; err != nil {
		c.JSON(http.StatusNotFound, gin.H{})
		return
	}

	// if we have TradeDetail it means that we did at least 1 successful order
	// if we got an error it means that we didn't any successful order and trade stored in waitingStore
	if err := pgClient.Model(&trade).Related(&tradeDetail).Error; err != nil {
		store.RemoveTrade(store.GetWaitingTradesStore(), trade, true)
	} else {
		store.RemoveTrade(store.GetClosingTradesStore(), trade, true)
	}

	trade.IsCancel = true
	if err := pgClient.Save(&trade).Error; err != nil {
		c.JSON(http.StatusBadRequest, gin.H{"detail": "can't save"})
	} else {
		c.JSON(http.StatusOK, trade)
	}
}

func GetTrades(c *gin.Context) {
	var trade []db.Trade
	pgClient := util.GetPgClient()
	if res := pgClient.Where("user_id = ?", c.Param("user_id")).Find(&trade); res.Error != nil {
		util.LogError(res.Error, "error getting list of trades #911")
		c.String(http.StatusInternalServerError, "something happens")
	} else {
		c.JSON(http.StatusOK, trade)
	}
}

func authMiddleWare(c *gin.Context) {
	head := c.GetHeader("Bot-Access-Header")
	if head == botAccessHeader {
		c.Next()
	} else {
		c.Status(http.StatusUnauthorized)
		c.Abort()
	}
}

func GetRouts() *gin.Engine {
	botAccessHeader = os.Getenv("BOT_ACCESS_HEADER")
	if os.Getenv("GIN_MODE") == gin.ReleaseMode {
		log.Println("production mode")
		gin.SetMode(gin.ReleaseMode)
	}
	router := gin.Default()
	router.Use(authMiddleWare)

	router.GET("/ping/", Ping)
	router.POST("/users/", AddUser)
	router.PATCH("/users/:user_id/", UpdateUser)
	router.GET("/users/:user_id/", GetUser)
	router.DELETE("/users/:user_id/", ClearUser)
	router.POST("/trades/", AddTrade)
	router.GET("/trades/:user_id", GetTrades)
	router.DELETE("/trades/:user_id/:trade_id", CancelTrade)

	return router
}
