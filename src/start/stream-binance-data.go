package start

import (
	"encoding/json"
	"fmt"
	"github.com/gorilla/websocket"
	"log"
	"strconv"
	"trade-bot/src/db"
	"trade-bot/src/store"
	"trade-bot/src/util"
)

type CurrencyWatch struct {
	Symbol       string
	BuyAt        string
	StopLoseAt   string
	TakeProfitAt string
}

var safeRealTimeStorage *store.SafeRealTimeBinanceStorage

func binanceWatcher(b *util.Broker, c *websocket.Conn, close chan bool) {
	realTimeStorage := store.GetStorage()
	closeGoroutine := false

	go func() {
		for {
			closeGoroutine = <-close
			if closeGoroutine {
				break
			}
		}
	}()

	for {
		if closeGoroutine {
			break
		}
		_, message, err := c.ReadMessage()

		if err != nil {
			log.Println("[ERROR] ReadMessage:", err)
		}

		var msg []store.BinanceSocketData
		err = json.Unmarshal(message, &msg)
		if err != nil {
			log.Println("[ERROR] Parsing:", err)
			continue
		}

		realTimeStorage.UpdateData(msg)
		// TODO: close broker if we need to stop robot.
		//if something {
		//	b.Publish(false)
		//	b.Stop()
		//	continue
		//}
		b.Publish(true)
	}
}
func ConnectBinanceStream() {
	symbol := "!ticker@arr" // All coins in array
	safeRealTimeStorage = store.GetStorage()
	closeWatcher := make(chan bool)
	address := fmt.Sprintf("wss://stream.binance.com:9443/ws/%s", symbol)

	var wsDialer websocket.Dialer
	wsConn, _, err := wsDialer.Dial(address, nil)
	if err != nil {
		panic(err)
	}
	defer wsConn.Close()

	log.Println("Dialed:", address)

	b := util.NewBroker()
	go b.Start()

	go binanceWatcher(b, wsConn, closeWatcher)
	wts := store.GetWaitingTradesStore()
	ats := store.GetActiveTradesStore()
	cts := store.GetClosingTradesStore()
	go waiting(b, wts)
	go active(b, ats) // TODO: not implemented. It contains template
	go closing(b, cts)
	messages := b.Subscribe()
	for msg := range messages {
		if msg.(bool) {
		} else {
			b.Unsubscribe(messages)
			break
		}
	}
}

func waiting(broker *util.Broker, wt *store.WaitingTrades) {
	messages := broker.Subscribe()
	pgClient := util.GetPgClient()
	for msg := range messages {
		if msg.(bool) {
			trades := priceCheck(wt, safeRealTimeStorage.GetDataMap(), "WAITING")
			if len(trades) > 0 {
				for i := range trades {
					trade := trades[i]
					user, err := store.GetUser(trade.UserID)
					if err != nil {
						util.LogError(err, "waiting listener #163")
					}
					_, err1 := OpenMarketPosition(&trade, user)
					if err1 != nil {
						pgClient := util.GetPgClient()
						if err := pgClient.Model(&trade).Update("is_cancel", true).Error; err != nil {
							msg := fmt.Sprintf("can't set \"is_cancel\" for trade(ID: %v) when open it #814",
								trade.ID)
							util.LogError(err, msg)
						}
						util.LogError(err1, "can't open order #478")
						store.RemoveTrade(wt, trade)
						return
					}
					if err := pgClient.Model(&trade).Updates(db.Trade{
						TradeDetailID: trade.TradeDetailID,
					}).Error; err != nil {
						util.LogError(err, "can't update trade with trade detail #884")
						store.RemoveTrade(wt, trade)
						return
					}
					wt.MoveTradeToClosingTrades(trade)
				}
			}
		} else {
			broker.Unsubscribe(messages)
			break
		}
	}
}

func active(broker *util.Broker, ipt *store.ActiveTrades) {
	//messages := broker.Subscribe()
	//for msg := range messages {
	//	if msg.(bool) {
	//		priceCheck(ipt, safeRealTimeStorage.GetDataMap())
	//	} else {
	//		broker.Unsubscribe(messages)
	//		break
	//	}
	//}
}

func closing(broker *util.Broker, ct *store.ClosingTrades) {
	messages := broker.Subscribe()
	for msg := range messages {
		if msg.(bool) {
			trades := priceCheck(ct, safeRealTimeStorage.GetDataMap(), "CLOSING")
			if len(trades) > 0 {
				for i := range trades {
					go func() {
						trade := trades[i]
						user, err := store.GetUser(trade.UserID)
						if err != nil {
							util.LogError(err, "closing listener #953")
						}
						_, err1 := CloseMarketPosition(&trade, user)
						if err1 != nil {
							util.LogError(err1, "can't close order #887")
							pgClient := util.GetPgClient()
							if err := pgClient.Model(&trade).Update("is_cancel", true).Error; err != nil {
								msg := fmt.Sprintf("can't set \"is_cancel\" for trade(ID: %v) when close it #844",
									trade.ID)
								util.LogError(err, msg)
							}
							store.RemoveTrade(ct, trades[i])
						} else {
							store.RemoveTrade(ct, trades[i])
						}
					}()
				}
			}
		} else {
			broker.Unsubscribe(messages)
			break
		}
	}
}

func priceCheck(storeLink store.TradesActions,
	prices map[string]store.BinanceSocketData, storeType string) []db.Trade {
	storeData := *store.GetStore(storeLink)
	trades := make([]db.Trade, 0)
	for symbol := range storeData {
		currentPrice, _ := strconv.ParseFloat(prices[symbol].CurrentDayClosePrice, 64)
		for userId := range storeData[symbol] {
			for tradeId := range storeData[symbol][userId] {
				trade := storeData[symbol][userId][tradeId]
				if trade.IsActive {
					continue
				}
				switch storeType {
				case "WAITING":
					switch trade.BuyDirection {
					case "DOWN":
						if currentPrice <= trade.Price {
							if tr, err := store.GetTrade(storeLink, trade.Symbol, trade.UserID, trade.ID, true);
								tr.IsActive && err == nil {
								continue
							} else if err != nil {
								util.LogError(err, "pricecheck waiting-down #932")
								continue
							}
							trade.IsActive = true
							store.UpdateTradeInfo(storeLink, trade)
							trades = append(trades, trade)
						}
					case "UP":
						if currentPrice >= trade.Price {
							if tr, err := store.GetTrade(storeLink, trade.Symbol, trade.UserID, trade.ID, true);
								tr.IsActive && err == nil {
								continue
							} else if err != nil {
								util.LogError(err, "pricecheck waiting-up #222")
								continue
							}
							trade.IsActive = true
							store.UpdateTradeInfo(storeLink, trade)
							trades = append(trades, trade)
						}
					}
				case "CLOSING":
					if currentPrice >= trade.TakePosition {
						if tr, err := store.GetTrade(storeLink, trade.Symbol, trade.UserID, trade.ID, true);
							tr.IsActive && err == nil {
							continue
						} else if err != nil {
							util.LogError(err, "priceCheck closing-take #175")
							continue
						}
						trade.IsActive = true
						store.UpdateTradeInfo(storeLink, trade)
						trades = append(trades, trade)
					} else if currentPrice <= trade.StopPosition {
						if tr, err := store.GetTrade(storeLink, trade.Symbol, trade.UserID, trade.ID, true);
							tr.IsActive && err == nil {
							continue
						} else if err != nil {
							util.LogError(err, "priceCheck closing-stop #123")
							continue
						}
						trade.IsActive = true
						trade.CloseByStop = true
						store.UpdateTradeInfo(storeLink, trade)
						trades = append(trades, trade)
					}
				}
			}
		}
	}
	return trades
}
