package start

import (
	"fmt"
	"github.com/go-redis/redis"
	"trade-bot/src/util"
	"github.com/jinzhu/gorm"
	"time"
	"net/http"
	"trade-bot/src/store"
	"trade-bot/src/db"
)

func InitRedis() {
	client := util.GetRedisClient(0)

	err := client.Set("key", "Hello world!", 0).Err()
	if err != nil {
		panic(err)
	}

	val, err := client.Get("key").Result()
	if err != nil {
		panic(err)
	}
	fmt.Println("key", val)

	val2, err := client.Get("key2").Result()
	fmt.Println(err)
	if err == redis.Nil {
		fmt.Println("key2 does not exist")
	} else if err != nil {
		panic(err)
	} else {
		fmt.Println("key2", val2)
	}
}

func InitServer() *http.Server {
	router := GetRouts()
	srv := http.Server{
		Handler:      router,
		Addr:         "0.0.0.0:8080",
		WriteTimeout: 15 * time.Second,
		ReadTimeout:  15 * time.Second,
	}

	return &srv
}

func InitStore() {
	wts := store.GetWaitingTradesStore()
	cts := store.GetClosingTradesStore()
	pgClient := util.GetPgClient()
	rows, err := pgClient.Model(&db.Trade{}).Where("is_done = false and is_cancel = false").Rows()
	if err != nil {
		util.LogError(err, "error init trades #208")
	} else {
		for rows.Next() {
			var trade db.Trade
			pgClient.ScanRows(rows, &trade)
			if trade.TradeDetailID == 0 {
				store.AddTrade(wts, trade, true)
			} else {
				store.AddTrade(cts, trade, true)
			}
		}
	}
}

func InitUsers() {
	pgClient := util.GetPgClient()
	rows, err := pgClient.Model(&db.User{}).Where("deleted_at IS NULL").Rows()
	if err != nil {
		util.LogError(err, "error init users #206")
	} else {
		for rows.Next() {
			var user db.User
			pgClient.ScanRows(rows, &user)
			store.AddUser(user)
		}
	}
}

func ConnectDataBase() *gorm.DB {
	return util.GetPgClient()
}