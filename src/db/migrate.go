package db

import (
	"os"
	"trade-bot/src/util"
	"fmt"
)

// for some dangerous actions.
func Migrate() {
	args := os.Args
	db := util.GetPgClient()
	defer db.Close()
	if len(args) > 1 && args[1] == "--save" {
		fmt.Println("migrate --save")
		db.AutoMigrate(&User{}, &Trade{}, &TradeDetail{}, &ErrorsLog{}, &PlacedOrder{})
	} else {
		fmt.Println("migrate --unsave")
		db.DropTable()
		db.AutoMigrate(&User{}, &Trade{}, &TradeDetail{}, &ErrorsLog{}, &PlacedOrder{})
	}
}
