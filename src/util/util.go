package util

import (
	"github.com/go-redis/redis"
	"github.com/jinzhu/gorm"
	"os"
	"fmt"
	_ "github.com/jinzhu/gorm/dialects/postgres"
)

type ErrorsLog struct {
	gorm.Model
	Text string
}

var redisClient *redis.Client
var pgClient *gorm.DB

func GetRedisClient(DB int) *redis.Client {

	if redisClient == nil {
		redisClient = redis.NewClient(&redis.Options{
			Addr:     os.Getenv("REDIS_URL"),
			Password: os.Getenv("REDIS_PASSWORD"),
			DB:       DB,
		})
	}

	return redisClient
}

func GetPgClient() *gorm.DB {
	if pgClient == nil {
		var err error
		params := fmt.Sprintf("host=%s port=%s user=%s dbname=%s password=%s sslmode=disable",
			os.Getenv("POSTGRES_HOST"),
			os.Getenv("PGPORT"),
			os.Getenv("POSTGRES_USER"),
			os.Getenv("POSTGRES_DB"),
			os.Getenv("POSTGRES_PASSWORD"),
		)
		pgClient, err = gorm.Open("postgres", params)
		if err != nil {
			panic(err)
		}
	}
	return pgClient
}

func LogError(caughtError error, helpId ...string) {
	var errText string
	if len(helpId) > 0 {
		errText = fmt.Sprintf("id: %v, error: %v", helpId[0], caughtError.Error())
	} else {
		errText = caughtError.Error()
	}
	errorLog := ErrorsLog{Text: errText}
	client := GetPgClient()
	if err := client.Create(&errorLog).Error; err != nil {
		panic(err)
	}
}
